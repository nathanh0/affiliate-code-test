<?php

namespace App\Console\Controllers\Affiliate;

use App\Console\Controllers\BaseConsoleController;
use App\Lib\Geo\Calculate;
use App\Lib\Geo\Types\Location;
use App\Lib\NoSql\GamblingCom\Affiliate\AffiliatesNoSql;
use App\Lib\NoSql\GamblingCom\Geo\OfficeLocationsNoSql;
use Symfony\Component\Console\Helper\Table;

/**
 * Class LocationConsoleController
 *
 * @package App\Console\Controllers\Affiliate
 */
class LocationConsoleController extends BaseConsoleController
{
    /**
     * Get affiliates closest to this location.
     * Dublin office will be used if a valid location is not provided.
     *
     * @param string|null $location The Office Location Name.
     *
     * @throws \Exception
     */
    public function affiliatesNearLocation(string $location = null)
    {
        /**
         * Get our office locations.
         */
        $office_locations = new OfficeLocationsNoSql();

        if (!$location) {
            $location = OfficeLocationsNoSql::DEFAULT_LOCATION;
        }

        /**
         * Get the requested office location.
         */
        $office = $office_locations->getLocation($location);

        if (is_null($office) || empty($office)) {
            throw new \Exception("The Office Location does not exist: {$location}");
        }

        /**
         * Get the office geo location for comparison.
         */
        $office_location = new Location($office->latitude, $office->longitude);

        /**
         * Get our affiliate locations.
         */
        $affiliates = new AffiliatesNoSql();

        /**
         * Calculate the distance in kilometers for all affiliates.
         */
        $affiliates->collection->transform(function ($affiliate) use ($office_location) {
            $affiliate->kilometers_distance = null;
            $affiliate_location = new Location($affiliate->latitude, $affiliate->longitude);
            $miles = Calculate::milesBetween($affiliate_location, $office_location);
            $affiliate->kilometers_distance = Calculate::convertMilesTo($miles, Calculate::KILOMETERS);

            return $affiliate;
        });

        /**
         * Get all affiliates <= kilometers distance from the office location.
         */
        $closest = $affiliates->collection
            ->where('kilometers_distance', '<=', 100)
            ->sortBy('affiliate_id');

        /**
         * Create a command line friendly table.
         */
        $table = new Table($this->console);

        /**
         * Create headers for the command line table.
         */
        $table->setHeaders([
            'Affiliate ID',
            'Name',
            'Kilometers Distance',
        ]);

        /**
         * Add all records to the command line table output.
         */
        $closest->each(function ($affiliate) use ($table) {
            $table->addRow([
                $affiliate->affiliate_id,
                $affiliate->name,
                $affiliate->kilometers_distance,
            ]);
        });

        /**
         * Render the command line output.
         */
        $table->render();
    }
}
