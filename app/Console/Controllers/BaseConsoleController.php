<?php

namespace App\Console\Controllers;

use Symfony\Component\Console\Helper\FormatterHelper;
use Symfony\Component\Console\Output\ConsoleOutput;

/**
 * Class BaseConsoleController
 *
 * @package App\Console\Controllers
 */
class BaseConsoleController
{
    /**
     * @var ConsoleOutput
     */
    protected ConsoleOutput $console;

    /**
     * @var FormatterHelper
     */
    protected FormatterHelper $formatter;

    /**
     * BaseConsoleController constructor.
     */
    public function __construct()
    {
        $this->console = new ConsoleOutput();
        $this->formatter = new FormatterHelper();
    }
}
