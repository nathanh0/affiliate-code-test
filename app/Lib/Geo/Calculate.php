<?php

namespace App\Lib\Geo;

use App\Lib\Geo\Types\Location;

/**
 * Class Calculate
 *
 * @package App\Lib\Geo
 */
class Calculate
{
    /**
     * Measurement names
     */
    const DEGREES    = 'degrees';
    const FEET       = 'feet';
    const KILOMETERS = 'kilometers';
    const METERS     = 'meters';
    const MILES      = 'miles';
    const YARDS      = 'yards';

    /**
     * Miles conversion units
     */
    const MILES_TO = [
        self::DEGREES => 69.09,
        self::FEET => 5280,
        self::KILOMETERS => 1.60934,
        self::METERS => 1609.34,
        self::YARDS => 1760,
    ];

    /**
     * Get the miles between two Location coordinates.
     *
     * @param Location $origin      The starting coordinate.
     * @param Location $destination The ending coordinate.
     *
     * @return float The distance between two coordinates in miles.
     */
    public static function milesBetween(Location $origin, Location $destination): float
    {
        $latitude_sin = sin(deg2rad($origin->latitude)) * sin(deg2rad($destination->latitude));
        $longitude_cos = cos(deg2rad($origin->latitude))
            * cos(deg2rad($destination->latitude))
            * cos(deg2rad($origin->longitude - $destination->longitude));

        return rad2deg(acos($latitude_sin + $longitude_cos)) * self::MILES_TO[self::DEGREES];
    }

    /**
     * Convert miles to another supported measurement.
     *
     * @param float  $miles Miles to convert.
     * @param string $to    Measurement to convert to.
     *
     * @return float|null The value of the converted measurement from Miles.
     * @throws \Exception When a measurement conversion is not supported.
     */
    public static function convertMilesTo(float $miles, string $to): ?float
    {
        switch ($to) {
            case self::DEGREES:
                return $miles / self::MILES_TO[self::DEGREES];
            case self::FEET:
                return $miles * self::MILES_TO[self::FEET];
            case self::KILOMETERS:
                return $miles * self::MILES_TO[self::KILOMETERS];
            case self::METERS:
                return $miles * self::MILES_TO[self::METERS];
            case self::YARDS:
                return $miles * self::MILES_TO[self::YARDS];
            default:
                throw new \Exception("Conversion measurement is not supported: {$to}");
        }
    }
}
