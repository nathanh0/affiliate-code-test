<?php

namespace App\Lib\Geo\Types;

/**
 * Class Location
 *
 * @package App\Lib\Geo\Types
 */
class Location
{
    /**
     * Latitude
     *
     * @var float
     */
    private float $latitude;

    /**
     * Longitude
     *
     * @var float
     */
    private float $longitude;

    /**
     * Location constructor.
     *
     * @param float|null $latitude
     * @param float|null $longitude
     */
    public function __construct(float $latitude = null, float $longitude = null)
    {
        $this->latitude = $latitude;
        $this->longitude = $longitude;
    }

    /**
     * Return allowed protected & private values.
     *
     * @param string $name
     *
     * @return float|null
     */
    public function __get(string $name): ?float
    {
        switch ($name) {
            case 'latitude':
                return $this->latitude;
            case 'longitude':
                return $this->longitude;
        }

        return null;
    }
}
