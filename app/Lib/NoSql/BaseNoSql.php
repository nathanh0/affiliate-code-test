<?php

namespace App\Lib\NoSql;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Storage;

/**
 * Class BaseNoSql
 *
 * @package App\Lib\NoSql
 */
class BaseNoSql
{
    /**
     * NoSql data storage collection
     *
     * @var Collection
     */
    public Collection $collection;

    /**
     * NoSql File relative to storage/app.
     *
     * @var string
     */
    protected string $json_file;

    /**
     * Structure of the NoSql data.
     *
     * @var array|string[]
     */
    protected array $structure;

    /**
     * BaseNoSql constructor.
     *
     * @throws \Exception
     */
    public function __construct()
    {
        if (isset($this->json_file)) {
            $json_file = "nosql/{$this->json_file}.json";
            $basename = basename($this->json_file);

            if (!Storage::exists($json_file)) {
                /**
                 * The NoSql file does not exist.
                 */
                $basename = basename($this->json_file);

                throw new \Exception("The NoSql file does not exist in storage: {$basename}");
            }

            try {
                $data = json_decode(Storage::get($json_file));
            } catch (\Exception $e) {
                /**
                 * The NoSql file is not a valid JSON file.
                 */
                throw new \Exception("The NoSql file is not a valid JSON file: {$basename}");
            }

            /**
             * Load the JSON file as NoSql.
             */
            $this->collection = new Collection($data);

            /**
             * Optionally typecast the loaded data.
             */
            $this->typecast();
        }
    }

    /**
     * Optionally apply a typecast to the collection which can help with strictness in data.
     * This can reduce typecast checking throughout code as some json will be string but
     * need to be numeric or float.
     */
    protected function typecast()
    {
        if (empty($this->structure) || $this->collection->isEmpty()) {
            return;
        }

        $this->collection->transform(function ($row) {
            foreach ($row as $key => $item) {
                if (!is_string($key)) {
                    continue;
                }

                if (isset($this->structure[$key])) {
                    switch (substr($this->structure[$key], 0, 3)) {
                        /**
                         * Typecast as a string.
                         */
                        case 'str':
                            $item = (string) $item;
                            break;
                        /**
                         * Typecast as an integer.
                         */
                        case 'int':
                            $item = intval($item);
                            break;
                        /**
                         * Typecast as a float.
                         */
                        case 'flo':
                            $item = floatval($item);
                            break;
                    }

                    $row->{$key} = $item;
                }
            }

            return $row;
        });
    }

    /**
     * Gets the structure for the NoSql data.
     *
     * @return array|string[]|null
     */
    public function getStructure(): ?array
    {
        return $this->structure;
    }
}
