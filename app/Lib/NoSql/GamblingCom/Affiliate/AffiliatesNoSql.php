<?php

namespace App\Lib\NoSql\GamblingCom\Affiliate;

use App\Lib\NoSql\BaseNoSql;

/**
 * Class AffiliatesNoSql
 *
 * @package App\Lib\NoSql\GamblingCom\Affiliate
 */
class AffiliatesNoSql extends BaseNoSql
{
    /**
     * NoSql File relative to storage/app.
     *
     * @var string
     */
    protected string $json_file = 'gamblingcom/affiliate/affiliates';

    /**
     * Structure of the NoSql data.
     *
     * @var array|string[]
     */
    protected array $structure = [
        'latitude' => 'float',
        'affiliate_id' => 'int',
        'name' => 'string',
        'longitude' => 'float',
    ];
}
