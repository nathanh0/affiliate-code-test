<?php

namespace App\Lib\NoSql\GamblingCom\Geo;

use App\Lib\NoSql\BaseNoSql;

/**
 * Class OfficeLocationsNoSql
 *
 * @package App\Lib\NoSql\GamblingCom\Geo
 */
class OfficeLocationsNoSql extends BaseNoSql
{
    /**
     * Default office location will be in Dublin.
     */
    const DEFAULT_LOCATION = 'Dublin';

    /**
     * NoSql File relative to storage/app.
     *
     * @var string
     */
    protected string $json_file = 'gamblingcom/geo/office-locations';

    /**
     * Structure of the NoSql data.
     *
     * @var array|string[]
     */
    protected array $structure = [
        'name' => 'string',
        'latitude' => 'float',
        'longitude' => 'float',
    ];

    /**
     * Get a Location by Name.
     *
     * @param string $location
     *
     * @return object|null
     */
    public function getLocation(string $location): ?object
    {
        return $this->collection->where('name', '=', $location)->first();
    }
}
