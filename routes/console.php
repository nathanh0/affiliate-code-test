<?php

use App\Console\Controllers\Affiliate\LocationConsoleController;
use Illuminate\Support\Facades\Artisan;

/*
|--------------------------------------------------------------------------
| Console Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of your Closure based console
| commands. Each Closure is bound to a command instance allowing a
| simple approach to interacting with each command's IO methods.
|
*/

/**
 * Get closest Affiliates to a Gambling.com Office Location.
 */
Artisan::command('affiliate:closest {location?}', function (string $location = null) {
    (new LocationConsoleController())->affiliatesNearLocation($location);
});
