<?php

namespace Tests\Feature\Console\Controllers\Affiliate;

use Illuminate\Support\Facades\Artisan;
use Tests\TestCase;

class LocationConsoleControllerTest extends TestCase
{
    const ARTISAN_COMMAND = 'affiliate:closest';

    public function testArtisanHasAffiliateClosestCommand()
    {
        $this->assertArrayHasKey(self::ARTISAN_COMMAND, Artisan::all());
    }

    public function testAffiliatesNearLocationArtisanCommandSuccess()
    {
        $this->artisan(self::ARTISAN_COMMAND, ['Dublin'])
            ->assertExitCode(0);
    }
}
