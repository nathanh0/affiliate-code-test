<?php

namespace Tests\Unit\Lib\Geo;

use App\Lib\Geo\Calculate;
use App\Lib\Geo\Types\Location;
use PHPUnit\Framework\TestCase;

class CalculateTest extends TestCase
{
    const TEST_LATITUDE  = 55.0000000;
    const TEST_LONGITUDE = -5.0000000;

    const TEST_DUBLIN_OFFICE_LATITUDE  = 53.3340285;
    const TEST_DUBLIN_OFFICE_LONGITUDE = -6.2535495;

    const TEST_FEET_FROM_DUBLIN_OFFICE       = 664061.4546703601;
    const TEST_KILOMETERS_FROM_DUBLIN_OFFICE = 202.40542830667;
    const TEST_METERS_FROM_DUBLIN_OFFICE     = 202405.42830666614;
    const TEST_MILES_FROM_DUBLIN_OFFICE      = 125.76921489969;
    const TEST_YARDS_FROM_DUBLIN_OFFICE      = 221353.81822345336;

    private Location $origin;
    private Location $destination;

    private function createTestLocationAndDublinLocation()
    {
        $this->origin = new Location(self::TEST_LATITUDE, self::TEST_LONGITUDE);
        $this->destination = new Location(self::TEST_DUBLIN_OFFICE_LATITUDE, self::TEST_DUBLIN_OFFICE_LONGITUDE);
    }

    public function testFeetBetweenTestCoordinatesAndDublin()
    {
        $this->createTestLocationAndDublinLocation();

        $this->assertEquals(
            self::TEST_FEET_FROM_DUBLIN_OFFICE,
            Calculate::convertMilesTo(
                Calculate::milesBetween($this->origin, $this->destination),
                Calculate::FEET
            )
        );
    }

    public function testKilometersBetweenTestCoordinatesAndDublin()
    {
        $this->createTestLocationAndDublinLocation();

        $this->assertEquals(
            self::TEST_KILOMETERS_FROM_DUBLIN_OFFICE,
            Calculate::convertMilesTo(
                Calculate::milesBetween($this->origin, $this->destination),
                Calculate::KILOMETERS
            )
        );
    }

    public function testMetersBetweenTestCoordinatesAndDublin()
    {
        $this->createTestLocationAndDublinLocation();

        $this->assertEquals(
            self::TEST_METERS_FROM_DUBLIN_OFFICE,
            Calculate::convertMilesTo(
                Calculate::milesBetween($this->origin, $this->destination),
                Calculate::METERS
            )
        );
    }

    public function testMilesBetweenTestCoordinatesAndDublin()
    {
        $this->createTestLocationAndDublinLocation();

        $this->assertEquals(
            self::TEST_MILES_FROM_DUBLIN_OFFICE,
            Calculate::milesBetween($this->origin, $this->destination)
        );
    }

    public function testYardsBetweenTestCoordinatesAndDublin()
    {
        $this->createTestLocationAndDublinLocation();

        $this->assertEquals(
            self::TEST_YARDS_FROM_DUBLIN_OFFICE,
            Calculate::convertMilesTo(
                Calculate::milesBetween($this->origin, $this->destination),
                Calculate::YARDS
            )
        );
    }
}
