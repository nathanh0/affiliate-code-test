<?php

namespace Tests\Unit\Lib\Geo\Types;

use App\Lib\Geo\Types\Location;
use PHPUnit\Framework\TestCase;

class LocationTest extends TestCase
{
    const TEST_LATITUDE  = 55.0000000;
    const TEST_LONGITUDE = -5.0000000;

    public function testLatitudeWithoutLongitudeTypeErrorException()
    {
        $this->expectExceptionMessage('Typed property App\Lib\Geo\Types\Location::$longitude must be float, null used');
        new Location(self::TEST_LATITUDE);
    }

    public function testLongitudeWithoutLatitudeTypeErrorException()
    {
        $this->expectExceptionMessage('Typed property App\Lib\Geo\Types\Location::$latitude must be float, null used');
        new Location(null, self::TEST_LONGITUDE);
    }

    public function testLatitudeIsFloat()
    {
        $location = new Location(self::TEST_LATITUDE, self::TEST_LONGITUDE);

        $this->assertIsFloat($location->latitude);
    }

    public function testLongitudeIsFloat()
    {
        $location = new Location(self::TEST_LATITUDE, self::TEST_LONGITUDE);

        $this->assertIsFloat($location->longitude);
    }

    public function testLatitudeInputMatchesOutput()
    {
        $location = new Location(self::TEST_LATITUDE, self::TEST_LONGITUDE);

        $this->assertEquals(self::TEST_LATITUDE, $location->latitude);
    }

    public function testLongitudeInputMatchesOutput()
    {
        $location = new Location(self::TEST_LATITUDE, self::TEST_LONGITUDE);

        $this->assertEquals(self::TEST_LONGITUDE, $location->longitude);
    }
}
