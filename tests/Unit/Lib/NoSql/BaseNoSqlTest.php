<?php

namespace Tests\Unit\Lib\NoSql;

use App\Lib\NoSql\BaseNoSql;
use Tests\TestCase;

class BaseNoSqlTest extends TestCase
{
    public function testBaseNoSqlHasCollectionObject()
    {
        $this->assertClassHasAttribute('collection', BaseNoSql::class);
    }

    public function testBaseNoSqlHasStructureObject()
    {
        $this->assertClassHasAttribute('structure', BaseNoSql::class);
    }

    public function collectionAppliedStructure(BaseNoSql $nosqls)
    {
        $nosql = $nosqls->collection->first();
        $structure = $nosqls->getStructure();

        foreach ($nosql as $key => $value) {
            $expected_type = $structure[$key];
            $actual_type = gettype($value);

            switch (substr($expected_type, 0, 3)) {
                case 'flo':
                    $this->assertEquals('double', $actual_type);
                    break;
                case 'int':
                    $this->assertEquals('integer', $actual_type);
                    break;
                case 'str':
                    $this->assertEquals('string', $actual_type);
                    break;
                default:
                    $this->fail("NoSql does not support structure type: {$actual_type}");
            }
        }
    }
}
