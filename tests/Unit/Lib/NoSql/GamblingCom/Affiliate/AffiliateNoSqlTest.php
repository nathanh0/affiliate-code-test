<?php

namespace Tests\Unit\Lib\NoSql\GamblingCom\Affiliate;

use App\Lib\NoSql\GamblingCom\Affiliate\AffiliatesNoSql;
use Illuminate\Support\Collection;
use Tests\Unit\Lib\NoSql\BaseNoSqlTest;

class AffiliateNoSqlTest extends BaseNoSqlTest
{
    public function testAffiliatesCollectionIsCollectionObject()
    {
        $affiliates = new AffiliatesNoSql();

        $this->assertEquals(Collection::class, get_class($affiliates->collection));
    }

    public function testAffiliatesCollectionContainsData()
    {
        $affiliates = new AffiliatesNoSql();

        $this->assertArrayHasKey(0, $affiliates->collection);
    }

    public function testAffiliatesCollectionAppliedStructure()
    {
        $affiliates = new AffiliatesNoSql();

        $this->collectionAppliedStructure($affiliates);
    }
}
