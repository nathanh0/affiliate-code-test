<?php

namespace Tests\Unit\Lib\NoSql\GamblingCom\Geo;

use App\Lib\NoSql\GamblingCom\Geo\OfficeLocationsNoSql;
use Illuminate\Support\Collection;
use Tests\Unit\Lib\NoSql\BaseNoSqlTest;

class OfficeLocationsNoSqlTest extends BaseNoSqlTest
{
    const TEST_LOCATION_NAME_DUBLIN = 'Dublin';

    public function testOfficeLocationsCollectionIsCollectionObject()
    {
        $locations = new OfficeLocationsNoSql();

        $this->assertEquals(Collection::class, get_class($locations->collection));
    }

    public function testOfficeLocationsCollectionContainsData()
    {
        $locations = new OfficeLocationsNoSql();

        $this->assertArrayHasKey(0, $locations->collection);
    }

    public function testOfficeLocationsCollectionAppliedStructure()
    {
        $locations = new OfficeLocationsNoSql();

        $this->collectionAppliedStructure($locations);
    }
}
